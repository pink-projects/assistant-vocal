# Assistant Météorologique Vocal

## Aperçu
Ce script Python est conçu pour récupérer les informations météorologiques d'une localité spécifique (Montreal dans ce cas) via l'API OpenWeatherMap, et pour annoncer les conditions météorologiques actuelles à l'aide de la synthèse vocale.

## Fonctionnalités
- **Récupération des données météorologiques :** Utilise l'API OpenWeatherMap pour obtenir des données telles que la température, l'humidité et la description du temps.
- **Synthèse vocale :** Annonce les conditions météorologiques à l'utilisateur en utilisant la bibliothèque pyttsx3, qui fonctionne hors ligne.

## Prérequis
- Python 3.x
- Bibliothèques Python :
  - `requests` pour les requêtes HTTP.
  - `json` pour la manipulation des données JSON.
  - `pyttsx3` pour la synthèse vocale.
- Clé API valide pour OpenWeatherMap.

Pour intégrer les directives d'installation en utilisant le fichier `requirements.txt` et mentionner la nécessité d'obtenir une clé API, voici comment vous pourriez modifier la section **Installation** du README :

---

## Installation

Pour installer et configurer correctement ce script, suivez ces étapes :

1. **Installer les dépendances :**
   Pour garantir que toutes les dépendances nécessaires sont correctement installées, utilisez le fichier `requirements.txt`. Ouvrez votre terminal et exécutez la commande suivante à l'emplacement du fichier :

   ```bash
   pip install -r requirements.txt
   ```

   Cette commande installera toutes les bibliothèques nécessaires avec les versions spécifiques listées dans le fichier, assurant ainsi la compatibilité des modules.

2. **Clé API :**
   Vous devez obtenir une clé API sur [OpenWeatherMap](https://openweathermap.org/) pour pouvoir faire des requêtes à leur service. Une fois obtenue, remplacez la valeur de la variable `key` dans le script avec votre clé API personnelle. Cette étape est cruciale pour l'accès aux données météorologiques en temps réel.

## Exécution du Script
Exécutez le script dans votre environnement Python. Le script fera une requête à l'API pour les données météorologiques de Montréal, enregistrera ces données dans un fichier JSON, et annoncera les conditions via la synthèse vocale.

## Structure du Code
- **Requête API :** Envoie une requête HTTP à OpenWeatherMap et traite la réponse.
- **Sauvegarde des données :** Les données JSON reçues sont sauvegardées localement dans un fichier `data.json`.
- **Lecture et annonce des données :** Les données sont lues à partir du fichier JSON et annoncées à l'utilisateur à l'aide de la synthèse vocale.

## Gestion des Erreurs
Le script inclut un traitement des erreurs pour les problèmes de réseau ou les erreurs liées à l'API, et affiche un message d'erreur approprié.
