import requests
import json
import pyttsx3
import os

city ="Montreal"
key= os.getenv('OPENWEATHER_API_KEY') 


def init_assistant(text):
    engine =pyttsx3.init()
    engine.say(text)
    engine.runAndWait()
try:
    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={key}")
    if response.status_code == 200:
        data = response.json()
        with open('data.json', 'w') as f:
            json.dump(data, f) 
    else:
        print(f"Erreur HTTP: {response.status_code}")
except Exception as e:
    print(e)

with open('data.json','r') as f:
    data=json.load(f)

    temperature = data['main']['temp']
    description=data['weather'][0]['description']
    feel =data['main']['feels_like']
    humidity=(data['main']['humidity'])

init_assistant(f"{city} - Temperature: {temperature} °C, Feels like: {feel} °C, Humidity: {humidity}%, Description: {description}")
